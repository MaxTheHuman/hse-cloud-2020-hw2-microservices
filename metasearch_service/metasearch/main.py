from flask import Flask, request, jsonify

import requests


app = Flask(__name__)

# geo_service = GeoService(CSV(GEO_DATA_FILE))
# print(user_service.get_user_data(0))

@app.route('/search', methods = ['GET'])
def search():
    text = request.args.get('text')
    user_id = int(request.args.get('user_id'))
    ip = request.args.get('ip_addr')
    if ip is None:
        ip = request.remote_addr

    user_data = curr_ip = requests.get('http://127.0.0.1:5001/get_user_data?user_id=' + str(user_id)).json()
    if user_data is not None:
        print("user_data:", user_data["age"], user_data["gender"])

    geo_data = curr_ip = requests.get('http://127.0.0.1:5002/get_geo_data?ip_addr=' + ip).json()
    if geo_data is not None:
        print("geo_data:", geo_data.get('region'))

    query_string = 'http://127.0.0.1:5003/get_search_data?text=' + text + '&age=' + str(user_data["age"])\
                                                                            + '&gender=' + user_data["gender"]\
                                                                            + '&region=' + geo_data["region"]
    print("query:", query_string)
    return requests.get(query_string).json()

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
