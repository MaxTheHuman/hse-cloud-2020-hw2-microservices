#!/bin/bash

docker container rm metasearch

docker build -t metasearch_service_image .

docker run -it --network=host -p 5000:5000 --name=metasearch metasearch_service_image

