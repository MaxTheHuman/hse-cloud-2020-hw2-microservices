#!/bin/bash

docker container rm geo

docker build -t geo_service_image .

docker run -it -p 5002:5000 --name=geo geo_service_image
