from flask import Flask, request, jsonify
from service import GeoService
from data_source import CSV

app = Flask(__name__)

GEO_DATA_FILE = "geo.csv"
geo_service = GeoService(CSV(GEO_DATA_FILE))
# print(user_service.get_user_data(0))
@app.route('/get_geo_data', methods = ['GET'])
def get_geo_data():
    if request.method == 'GET':
        arguments = request.args
        if "ip_addr" in arguments:
            # print("id to search:", arguments["user_id"])
            # print("data:", user_service.get_user_data(int(arguments["user_id"])))
            return jsonify(geo_service.get_geo_data(arguments["ip_addr"]))
        else:
            return jsonify({"error": "you should pass ip address as ip_addr url argument"})
        # return jsonify(user_service.get_user_data(user_id))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
