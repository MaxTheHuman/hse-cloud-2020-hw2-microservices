#!/bin/bash

docker container rm user

docker build -t user_service_image .

docker run -it -p 5001:5000 --name=user user_service_image
