from flask import Flask, request, jsonify
from service import UserService
from data_source import CSV

app = Flask(__name__)

USER_DATA_FILE = "users.csv"
user_service = UserService(CSV(USER_DATA_FILE))
# print(user_service.get_user_data(0))
@app.route('/get_user_data', methods = ['GET'])
def get_user_data():
    if request.method == 'GET':
        arguments = request.args
        if "user_id" in arguments:
            # print("id to search:", arguments["user_id"])
            # print("data:", user_service.get_user_data(int(arguments["user_id"])))
            return jsonify(user_service.get_user_data(int(arguments["user_id"])))
        else:
            return jsonify({"error": "you should pass user id as user_id url argument"})
        # return jsonify(user_service.get_user_data(user_id))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
