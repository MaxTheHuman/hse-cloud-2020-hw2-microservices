from flask import Flask, request, jsonify
from service import SimpleSearchService
from data_source import CSV

app = Flask(__name__)

NEWS_DATA_FILE = "news_generated.csv"
simple_search_service = SimpleSearchService(CSV(NEWS_DATA_FILE))
# print(user_service.get_user_data(0))
@app.route('/get_search_data', methods = ['GET'])
def get_search_data():
    if request.method == 'GET':
        arguments = request.args
        if "text" in arguments:
            text = arguments.get('text')
            print("text:", text)
        if "region" in arguments:
            region = arguments.get('region')
            print("region:", region)
        else:
            region = ""
        if "age" in arguments:
            age = arguments.get('age')
            print("age:", age)
        else:
            age = 0
        if "gender" in arguments:
            gender = arguments.get('gender')
            print("gender:", gender)
        else:
            gender = ""
        user_data = {"age": age, "gender": gender}
        geo_data = {"region": region}

        return simple_search_service.get_search_data(text, user_data, geo_data).to_json()

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
