#!/bin/bash

docker container rm search

docker build -t search_service_image .

docker run -it -p 5003:5000 --name=search search_service_image
